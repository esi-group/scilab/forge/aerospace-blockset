//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_timeframe_conversion(block,flag)

previousprot = funcprot(0)

previousprot = funcprot(0)

block_name = "AB_timeframe_conversion";
block_label = "TIMEFRAME_CONVERSION";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    timeframe_from = block.opar(1);
    timeframe_to = block.opar(2);
    value_from = block.inptr(1);
	block.outptr(1) = CL_dat_convert(timeframe_from, timeframe_to, value_from);
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 1, 0);
    timeframe_from = block.opar(1);
    timeframe_to = block.opar(2);
    if and(timeframe_from <> ["cal" "mjd" "jd" "cjd"]) then
      error(msprintf(gettext("\t%s : Incorrect timeframe to convert from%s\n"),..
      block_name, timeframe_from)); 
    end
    if and(timeframe_to <> ["cal" "mjd" "jd" "cjd"]) then
      error(msprintf(gettext("\t%s : Incorrect timeframe to convert to%s\n"),..
      block_name, timeframe_from)); 
    end
    if timeframe_from == timeframe_to then
      error(msprintf(gettext("\t%s : Conversion between the same time frames\n"),..
      block_name)); 
    end
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
