//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_unit_conversion(block,flag)

previousprot = funcprot(0);

block_name = "AB_unit_conversion";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    input = block.inptr(1);
    f1 = block.oz(1);
    f2 = block.oz(2);
    
    block.outptr(1) = input * f1 / f2;
  
  case 2 then
    ;
    
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 1, 0);
    unit_from = block.opar(1);
    unit_to = block.opar(2);
    
    // Units verification
    if and(unit_from <> ["deg" "rad" "km" "m" "au" "m/s" "km/s" "au/s" "m/mn" "km/mn" "au/mn" "m/h" "km/h" "au/h" "m/day" "km/day" "au/day" "m/yr" "km/yr" "au/yr"]) then
      error(msprintf(gettext("\t%s : Incorrect value of model.opar(1)\n"),..
      block_name)); 
    end
    if and(unit_from <> ["deg" "rad" "km" "m" "au" "m/s" "km/s" "au/s" "m/mn" "km/mn" "au/mn" "m/h" "km/h" "au/h" "m/day" "km/day" "au/day" "m/yr" "km/yr" "au/yr"]) then
      error(msprintf(gettext("\t%s : Incorrect value of model.opar(2)\n"),..
      block_name));
    end
    if unit_from == unit_to then
      error(msprintf(gettext("\t%s : Conversion between the same units\n"),..
      block_name)); 
    end
    
    //Coefficient calculation
    // Declarations:
    global %CL__PRIV; 
    if (~exists("%CL_au")); %CL_au = %CL__PRIV.DATA.au; end
    
    // Code:
    // reference units
    m   = 1;
    s   = 1;
    kg  = 1;
    rad = 1;
    Hz  = 1;

    // derived units
    km  = 1000      * m;
    au  = %CL_au    * m;  // NB: %CL_au must be defined in meters ! 
    h   = 3600      * s;
    mn  = 60        * s;
    day = 86400     * s;
    yr =  365.25    * day;
    g   = 1.e-3     * kg;
    deg = (%pi/180) * rad;
    N   = 1         * kg*m/s^2;
    kHz = 1000      * Hz;
    arcmin = (1/60) * deg; 
    arcsec = (1/3600) * deg; 
    mas = (1/1000) * arcsec; 
    
    // The two following lines are equivalent to 
    // f1 = evstr(unit1); 
    // f2 = evstr(unit2); 
    // except that errors can be retrieved properly (avoid try/catch)
    ier1 = execstr("f1 = " + unit_from, "errcatch");
    ier2 = execstr("f2 = " + unit_to, "errcatch");
    
    if (ier1 == 0 & ier2 == 0)
      block.oz(1) = f1;
      block.oz(2) = f2;
    else
      CL__error("Unable to compute (possibly invalid units).")
    end
    
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
