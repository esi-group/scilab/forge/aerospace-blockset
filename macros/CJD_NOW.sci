//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2014 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=CJD_NOW(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
   case 'define' then
    model	= scicos_model();
    model.sim	= list('AB_cjd_now',5);
    // one input with a six keplerian "double" elements
    model.in	= [];
    model.in2	= [];
    model.intyp	= [];
    // two outputs with a thre cartesian "double" elements
    model.out	= [1];
    model.out2	= [1];
    model.outtyp= [1];
    // block parameters
    model.ipar	= [];
    model.rpar	= [];
    model.opar	= list();    

    model.blocktype='c';
    model.dep_ut=[%f %t];

    exprs=string([]);
    gr_i=['txt=[''CJD_NOW''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

