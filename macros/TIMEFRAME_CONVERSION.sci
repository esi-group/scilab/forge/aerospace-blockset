//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=TIMEFRAME_CONVERSION(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    from = arg1.graphics.exprs(1);
    to = arg1.graphics.exprs(2);
    label = "$\mbox{" + from + "}\rightarrow\mbox{" + to + "}$";
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Convert from:';
                'Convert to:';];
      types = list( 'str', 1, 'str', 1);
      [ok, timeframe_from, timeframe_to, ..
          exprs]=getvalue(["Set time frames to convert"; "cjd  - modified juliand day since 1950"; "jd   - julian day"; "mjd  - modified juliand day (jd - 2400000.5)"; "cal  - calendar date"],labels, types, exprs);

      if ~ok then break,end

      //check validity of parameters
      if and(timeframe_from <> ["cal" "mjd" "jd" "cjd"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Convert from")), ..
          gettext("Value must be a time frame name (cal, cjd, mjd, jd)."));
        ok=%f
      end 
      if and(timeframe_to <> ["cal" "mjd" "jd" "cjd"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Convert to")), ..
          gettext("Value must be a time frame name (cal, cjd, mjd, jd)."));
        ok=%f
      end
      if (timeframe_from == timeframe_to) then
        block_parameter_error(msprintf(gettext("Cannot convert between the same time frames")), gettext("Use different frames for conversion"));
        ok=%f
      end

      if ok then
	if (timeframe_from == "cal") then
	    model.in = [6];
	else
	    model.in = [1];
	end
	if (timeframe_to == "cal") then
	    model.out = [6];
	else
	    model.out = [1];
	end
        model.opar = list(timeframe_from, timeframe_to);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    timeframe_from = "cjd";
    timeframe_to   = "cal";

    model           = scicos_model()
    model.sim       = list('AB_timeframe_conversion',5)
    model.in        = [1]
    model.in2       = [1]
    model.intyp     = [1]
    model.out       = [6]
    model.out2      = [1]
    model.outtyp    = [1]
    model.opar      = list(timeframe_from, timeframe_to);
    model.blocktype = 'c'
    model.dep_ut    = [%t %f]
    gr_i=['dx=sz(1)/5;dy=sz(2)/10;'
      'w=sz(1)-2*dx;h=sz(2)-2*dy;'
      'txt=label;'
      'xstringb(orig(1)+dx,orig(2)+dy,txt,w,h,''fill'');'
      'warning(''redrawing block'')']
    exprs=[
           timeframe_from;
           timeframe_to
          ]
    //gr_i=['dx=sz(1)/5;dy=sz(2)/10;';
    //  'w=sz(1)-2*dx;h=sz(2)-2*dy;';
    //  'txt=label;'
    //  'xstringb(orig(1)+dx,orig(2)+dy,txt,w,h,''fill'');']
    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

