//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_plot_polar(block,flag)
previousprot = funcprot(0);

block_name = "AB_plot_polar";
block_label = "PLOT_POLAR";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end
max_delay = 2;	// max delay in seconds between refreshes

select flag
  case 1 then
  // Output computation
    // Figure needs to be created or reused before any color() call to 
    // avoid creating empty graphic window
    f = findobj("Tag", block.uid);
    scf(f);
    s = size(block.opar(3));

    if f.user_data == []
	for i=1:s(2)
	    colors(i) = color(block.opar(3)(i));
	end
        AB_initialize_polar(f, block);
	for i=1:s(2)
	    plot2d(%inf,%inf, style=colors(i), axesflag=0);
	    e = gce();
	    e.children.thickness = 3;
            f.user_data = [f.user_data ; var2vec(e.children)];
	end
	a = f.children;
    end

    for k=1:s(2)
	mask = block.inptr(1)(4);
	azim = block.inptr(k+1)(1);
	elev = block.inptr(k+1)(2);
	p =  vec2var(f.user_data((k-1)*3+1:(k-1)*3+3));		// recover 3x1 vector from the nx1
        iter = size(p.data, "r");
	if elev >= mask
	    if p.data(iter) == [%inf,%inf]
		p.data = [%inf, %inf];
	    end	    
	    x = -(%pi/2-elev)*sin(azim);
	    y =  (%pi/2-elev)*cos(azim);
	    p.data = [p.data; x,y];
	    //plot2d(x,y, -1, axesflag=0);
	else
	    if p.data(iter) <> [%inf,%inf];
		p.data = [p.data; %inf,%inf];
	    end
	end
    end

  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 1, 1, 0);
    // Figure needs to be created or reused before any color() call to 
    // avoid creating empty graphic window
    f = findobj("tag", block.uid);
    if f == [] then
      f = figure("tag", block.uid, "toolbar","none");
      f.background = color("white");
      f.figure_name = "Azimuth/elevation polar plot";
      f.info_message = "Simulation in progress..."
    end  
    scf(f);
    clf();
    a = f.children;     
    a.user_data = [];
    f.user_data = [];
  case 5 then
  // Ending
    f = findobj("Tag", block.uid);
    scf(f);
    //a = f.children;
    //[names, colors, count] = get_parameters(block);
    //drawlater();
    f.info_message = "Simulation ended"
    //drawnow();
    //a.user_data = [];
    //f.user_data = [];
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
  end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction

function AB_initialize_polar(f, block)
    drawlater();

    //CL_init();
    try
      scf(f);
      a = gca();
    catch
      set_blockerror(-3);
      error("Failed to initialize the polar plot figure");
    end

    // Initialize parameters
    ground_color = color(block.opar(4));
    sky_color = color(block.opar(5));
    mask = block.inptr(1)(4);
    station_name = block.opar(1);
    object_name = block.opar(2)
    color_name = block.opar(3);
    s = size(color_name, "*");

    if (mask >= %pi/2) | (mask <= - %pi/2)  then
      messagebox("Mask must be greater than -PI/2 and less than PI/2 radians (positive up).", block_label, "error");
      set_blockerror(-1);
    end

    // Calculate captions
    lat = msprintf("%d deg", CL_rad2deg(block.inptr(1)(1)));
    lon = msprintf("%d deg", CL_rad2deg(block.inptr(1)(2)));
    alt = msprintf("%d m", block.inptr(1)(3));
    mas = msprintf("%d deg", CL_rad2deg(mask));

    // draw info
    xset("font",1,5);              // change font size
    xstring(2,-1, station_name);   // print the station name
    xset("font",1,3);              // change font size
    xstring(2,-1.8, ["latitude";"longitude";"altitude";"mask"]);
    xstring(3,-1.8, [lat;lon;alt;mas]);

    // isoview scaling 
    plot2d(0,0,-1,"031"," ",[-%pi/2-0.2,-%pi/2-0.2,3.5,%pi/2+0.2]);
    xset("color", ground_color);
    xfarc(-%pi/2,%pi/2,%pi,%pi,0,360*64);
    xset("color", sky_color);
    xfarc(-%pi/2+mask,%pi/2-mask,%pi-2*mask,%pi-2*mask,0,360*64);

    // draw legend
    xset("font",1,3); 
    for i = 1:s
	xstring(2.6,2-0.3*i, object_name(i));
	xset("color",color(color_name(i)));
	xfrect(2,2.15-0.3*i,0.5,0.15);
    end

    // draw captions
    xset("font",1,4);              // change size font
    xstring(-0.1,%pi/2+0.1,"N");
    xstring(0,-%pi/2-0.3,"S");
    xstring(-%pi/2-0.4,-0.1,"W");
    xstring(%pi/2+0.1,-0.1,"E");

    xset("font",1,1);              // change size font
    xstring(0.05,+0.1,"90");
    xstring(0.05,-%pi/6+0.1,"60");
    xstring(0.05,-%pi*2/6+0.1,"30");
    xstring(0.05,-%pi*3/6+0.1,"0");

    // draw axes lines
    xset("color", 1);
    xset("line style",0.5);
    xpoly([-%pi/2,%pi/2],[0,0]);
    xpoly([0,0],[-%pi/2,%pi/2]);
    xarc(-%pi/2,%pi/2,%pi,%pi,0,360*64);
    xarc(-%pi*1/6,%pi*1/6,%pi*2/6,%pi*2/6,0,360*64);
    xarc(-%pi*2/6,%pi*2/6,%pi*4/6,%pi*4/6,0,360*64);
    a.axes_visible = ["off","off","off"];
    a.margins = [0,0,0,0];
    a.box = "off";
    a.auto_scale = "off";

    drawnow();
endfunction
