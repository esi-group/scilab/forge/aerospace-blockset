//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2013 - Pawel Zagorski
// see license.txt for more licensing informations

function [graphics]=AB_set_block_style(graphics, sty)
select sty
	case 'default' then
		//rectangular white block
		graphics.style = "fillColor=white";
	else
		error(36, 2);
	end
endfunction
