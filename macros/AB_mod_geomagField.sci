//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_mod_geomagField(block,flag)

previousprot = funcprot(0)

block_name = "AB_mod_geomagField";
block_label = "GEOMAG_MODEL";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    pos_ECF = block.inptr(1);
    t_cjd = block.inptr(2);
    altitude = sqrt(pos_ECF(1)^2 + pos_ECF(2)^2 + pos_ECF(3)^2);
    if (altitude < 0) | (altitude > 42000000)
      set_blockerror(-1);      
      messagebox("Altitude must be between 0 and 42 000 000 meters for IGRF model to give precise values", block_label, "error"); 
    else
      block.outptr(1) = CL_mod_geomagField(t_cjd, pos_ECF);
    end
  case 4 then
  // Initialization
    if block.nin<>2 then
      set_blockerror(-1);      
      messagebox("Incorrect number of block inputs or output for calling CL_mod_geomagField() function", block_label, "error")
    end
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction

