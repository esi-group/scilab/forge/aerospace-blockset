//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=PLOT_GROUNDTRACK(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Accept herited events (0/1)';
                'Map color';
                'Map line thickness';
                'Trajectory color';
                'Trajectory line thickness'; 
                'Grid size in longitude and latitude (deg)';
                'Definition of the view area';
                'Resolution (low or high)';
                'Coordinates (sph=spherical, ell=elliptical)'];
      types = list('col', 1, 'str', 1, 'col', 1, 'str', 1, 'col', 1,..
                   'row', 2, 'mat', [2 2], 'str', 1, 'str', 1);
      [ok, herited, map_color_name, thickness, traj_color_name, traj_thickness,..
       steps, area, res, coord, exprs]=scicos_getvalue(..
       "Set PLOT_GROUNDTRACK block parameters",labels, types, exprs);

      if ~ok then break,end
      mess = [];
      if herited<>0 & herited<>1 then
        mess=[mess;'Accept herited events must have value of either 1 or 0';' '];
        ok=%f;
      end
      try
	map_color = color(map_color_name);
      catch
        mess=[mess;'Map color must be a string accepted by color() function.';' '];
        ok=%f
      end
      if thickness < 0 then
        mess=[mess;'Map line thickness cannot be a negative number';' '];
        ok=%f;
      end
      try
	traj_color = color(traj_color_name);
      catch
        mess=[mess;'Trajectory color must be a string accepted by color() function.';' '];
        ok=%f
      end
      if traj_thickness < 0 then
        mess=[mess;'Trajectory line thickness thickness cannot be a negative number';' '];
        ok=%f;
      end
      if steps(1) <= 0 | steps(2) <= 0 then
        mess=[mess;'Both grid sizes must be greater than 0';' '];
        ok=%f;
      end
      if res <> "low" & res <> "high" then
        mess=[mess;'Resolution must be either low or high';' '];
        ok=%f;
      end
      if coord <> "sph" & coord <> "ell" then
        mess=[mess;'Coordinates must be either sph or ell';' '];
        ok=%f;
      end
      if (area(2,1)<=area(1,1) | area(2,1)>area(1,1)+360) then
        mess=[mess;'Invalid view area bounds in longitude';' '];
        ok=%f;
      end
      if (area(2,2)<=area(1,2) | area(1,2) < -90 | area(2,2) > 90) then
        mess=[mess;'Invalid view area bounds in latitude';' '];
        ok=%f;
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess]);
      end

      if ok then
        [model,graphics,ok] = set_io(model,graphics,list([3 1],..
          [1]),list(),ones(1-herited,1),[])
      end

      if ok then
        model.ipar = [thickness, traj_thickness, map_color, traj_color, herited];
        model.opar = list(steps, area, res, coord);
        model.evtin= ones(1-herited,1);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end

   case 'define' then
    herited = 1;
    map_color=2;
    thickness=1;
    traj_color=5;
    traj_thickness = 2;
    steps=[30, 15];
    area=[-180, -90; 180, 90];
    res='low';
    coord='sph';

    model=scicos_model();
    model.sim=list('AB_plot_groundtrack',5);
    // oneinputs with a three "double" elements
    model.in=[3];
    model.in2=[1];
    model.intyp=[1];
    // no outputs with a single "double" element
    model.out=[];
    model.out2=[];
    model.outtyp=[];
    
    model.blocktype='c';
    model.dep_ut=[%t %f];

    model.ipar = [thickness, traj_thickness, map_color, traj_color, herited];
    model.opar = list(steps, area, res, coord);

    model.evtin = ones(1-herited,1)

    exprs=[
           string(herited);
           "blue";
           string(thickness);
           "red";
           string(traj_thickness);
           sci2exp(steps);
           '[-180, -90; 180, 90]';
           res;
           coord
          ]

    gr_i=['txt=[''Plot_Groundtrack''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

