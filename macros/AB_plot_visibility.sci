//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_plot_visibility(block,flag)

previousprot = funcprot(0)

block_name = "AB_plot_visibility";
block_label = "PLOT_VISIBILITY";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end
max_delay = 2;	// max delay in seconds between refreshes

select flag
  case 1 then
  // Output computation
    //Get initial parameters
    f = vec2var(block.oz(1));
    scf(f);
    a = f.children;
    [names, colors, count] = AB__get_pv_params(block);
    current_time = getscicosvars("t0");
    previous_time = f.user_data(1)(count+1);
    time_label = AB__get_pv_time_label(block.inptr(count+1), block_name);
    previous_time_label = f.user_data(2);
    arrow_size = 1;
    arrow_color = 1;
    redraw = 0;
    // drawlater();

    if current_time == 0 then
	// Add first x tick
	new_ticks = a.x_ticks;
	new_ticks(2) = [current_time];
	new_ticks(3) = [time_label];
	a.x_ticks = new_ticks;
    end

    //Iterate input sources
    for i=1:count
	previous_state = f.user_data(1)(i);
        current_state = block.inptr(i);

	// If object became visible, create rectangle
	if (current_state > 0) & (previous_state <= 0) then
	  scf(f);
	  color_id = color(colors(i));
	  plot2d([current_time], [i]);
          h = CL_g_select(gce(), "Polyline");
	  h.foreground = color_id; 
	  h.thickness = 10;
	  h.tag = block.uid + string(i);
	  new_ticks = a.x_ticks;
	  new_ticks(2) = [new_ticks(2); current_time];
	  new_ticks(3) = [new_ticks(3); time_label];
	  a.x_ticks = new_ticks;
	  a.user_data(i) = current_time;
	  redraw = 1;
	end

	// If object is still visible expand rectangle
	if (current_state > 0) & (previous_state > 0) then
	  a.user_data(i) = current_time;
	  last_refresh = f.user_data(3);
          now = getdate("s");
	  if now - last_refresh >= max_delay then
	    redraw = 1;
	  end
	end

	// If object no longer visible draw arrow
	if (current_state <= 0) & (previous_state > 0) then
          h = findobj("tag", block.uid + string(i));
	  h.data = [h.data; [a.user_data(i), i]];
          h.tag = "";
	  new_ticks = a.x_ticks;
	  new_ticks(2) = [new_ticks(2); previous_time];
	  new_ticks(3) = [new_ticks(3); previous_time_label];
	  a.x_ticks = new_ticks;
	  redraw = 1;
	end
	f.user_data(1)(i) = current_state;
end

    // Set current time as previous
    f.user_data(1)(count+1) = current_time;
    f.user_data(2) = time_label;

    // Redraw the plot if something changed
    if redraw == 1 then
      for i=1:count
	current_state = block.inptr(i);
	if current_state > 0 then
	  h = findobj("tag", block.uid + string(i));
	  if h <> [] then
	    h.data = [h.data(1,:); [current_time, i]];
	  else
	    set_blockerror(-3);
	    error(msprintf(gettext("%s : Could not find a line to redraw. %d"), block_name, flag));
	  end
	end
      end
      f.user_data(3) = getdate("s");
      // drawnow();
    end

  case 2 then
	;
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 1, 1, 0);

    // Figure needs to be created or reused before any color() call to 
    // avoid creating empty graphic window
    f = findobj("tag", block.uid);
    if f == [] then
      f = figure("tag", block.uid);
      f.background = color("white");
      f.figure_name = "Access/Visibility times";
      f.info_message = "Simulation in progress..."
    else
      clf(f);
    end       
    scf(f);
    block.oz(1) = var2vec(f);
    drawlater();
    [names, colors, count] = AB__get_pv_params(block);
    final_time = getscicosvars("tf");

    a = f.children;
    a.axes_visible = ["on" "on" "off"];
    a.axes_reverse = ["off" "on" "off"];
    a.grid = [1,-1];
    set(a,"box","on");
    a.data_bounds = [0,0;final_time,count+1];
    a.sub_ticks = [0,0];
    a.font_size = 2;
    title("Access times",'fontsize',3);
    xlabel("time",'fontsize',3);
    ylabel("event",'fontsize',3);

    new_ticks = a.y_ticks;
    new_ticks(2) = (1:1:count)';
    new_ticks(3) = names';
    a.y_ticks = new_ticks;

    // Initialize storage of previous states
    last_refresh = getdate("s");
    f.user_data = list(zeros(count+1, 1),"time", last_refresh);  

    // Reset x ticks
    new_ticks = a.x_ticks;
    new_ticks(2) = [];
    new_ticks(3) = [];
    a.x_ticks = new_ticks;

    drawnow();
  case 5 then
  // Ending
    f= findobj("tag", block.uid);
    scf(f);
    a = f.children;
    current_time = getscicosvars("t0");
    time_label = f.user_data(2);
    [names, colors, count] = AB__get_pv_params(block);
    drawlater();
    // Finish unfinisched rectangles
    for i=1:count
	previous_state = f.user_data(1)(i);
        if (previous_state > 0) then
	  h = findobj("tag", block.uid + string(i));
	  h.data = [h.data; [current_time, i]];
	  h.tag = "";	  
	end
    end
    // Add final x tick
    new_ticks = a.x_ticks;
    new_ticks(2) = [new_ticks(2); current_time];
    new_ticks(3) = [new_ticks(3); time_label];
    a.x_ticks = new_ticks;
    f.info_message = "Simulation ended"
    drawnow();
    a.user_data = [];
    f.user_data = [];
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
