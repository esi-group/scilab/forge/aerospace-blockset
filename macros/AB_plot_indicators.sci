//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_plot_indicators(block,flag)

previousprot = funcprot(0)

block_name = "AB_plot_indicators";
block_label = "PLOT_INDICATORS";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end
max_delay = 2;	// max delay in seconds between refreshes

select flag
  case 1 then
  // Output computation
    //Get initial parameters
    f = findobj("tag", block.uid);
    scf(f);
    a = f.children;
    count = size(block.inptr);
    col = [color(block.opar(4)(1)), color(block.opar(4)(2)), color(block.opar(4)(3))];
    name = block.opar(1);
    //drawlater();

    if f.user_data == []
        xset("font", 1, 3);
    	a.tight_limits="on";
    	a.data_bounds = [0,0;5,count];
    	f.axes_size = [200, 50*count]
    	for i=1:count
    	    xfrect(0.5,i-0.1,4,0.8);
    	    rec = var2vec(gce());
    	    xstringb(0.5,(i-1)+0.1,name(i),4,0.8);
    	    xset("color",1)
    	    xrect(0.5,i-0.1,4,0.8);
    	    f.user_data = [f.user_data; rec];
    	end
    end

    //Iterate input sources
    for i=1:count
	pos_level = block.opar(2)(i);
	neg_level = block.opar(3)(i);
	rec = vec2var(f.user_data((i-1)*3+1:(i-1)*3+3));
	if block.inptr(i) >= pos_level
	    rec.background = col(1);
	elseif block.inptr(i) > neg_level
	    rec.background = col(2);
	else
	    rec.background = col(3);
	end
    end

    //drawnow();

  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 1, 1, 0);

    // Figure needs to be created or reused before any color() call to 
    // avoid creating empty graphic window
    f = findobj("tag", block.uid);
    if f == [] then
      f = figure("tag", block.uid, "toolbar","none", "menubar", "none", "resize", "off");
      f.background = color("white");
      f.figure_name = "Indicators";
      f.info_message = "Simulation in progress..."
    end       
    scf(f);
    drawlater();
    final_time = getscicosvars("tf");

    a = f.children;
    a.axes_visible = ["off" "off" "off"];
    a.axes_reverse = ["off" "on" "off"];
    //a.grid = [1,-1];
    set(a,"box","off");
    a.font_size = 2;
    //title("Indicators",'fontsize',3);

    drawnow();

  case 5 then
  // Ending
    f= findobj("tag", block.uid);
    scf(f);
    a = f.children;
    f.info_message = "Simulation ended"
    a.user_data = [];
    f.user_data = [];
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
