//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2013 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=ECLIPSE_MODEL(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Radius of eclipsed body';
                'Radius of eclipsing body';
		'Accept herited events (0/1)'];
      types = list( 'row', 1, 'row', 1, 'col', 1);
      [ok, radius_bright, radius_dark, herited, ..
	 exprs]=getvalue(["Set radius of eclipsed and eclipsing body."; "Default values are those of the Sun and the Earth."],labels, types, exprs);

      if ~ok then break,end
      mess=[]
      if herited<>0 & herited<>1 then
        mess=[mess;'Accept herited events must have value of either 1 or 0';' ']
        ok=%f
      end
      if radius_bright<=0 | radius_dark<=0 then
        mess=[mess;'Radii of both bodies must be greater than 0.';' ']
        ok=%f
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end

      if ok then
        model.evtin= ones(1-herited,1);
        model.opar = list(radius_bright, radius_dark, herited);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model;
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    radius_bright = CL_dataGet("body.Sun.eqRad");
    radius_dark   = CL_dataGet("body.Earth.eqRad");

    model           = scicos_model();
    model.sim       = list('AB_gm_eclipseCheck',5);
    model.in        = [ 3;  3;  3];
    model.in2       = [-2; -2; -2];
    model.out       = [ 1;  1];
    model.out2      = [-2; -2];
    model.outtyp    = [ 1];
    model.evtin	    = [ 1];
    model.opar      = list(radius_bright, radius_dark);
    model.blocktype = 'c';
    model.dep_ut    = [%t %f];
    exprs=[
           string(radius_bright);
           string(radius_dark);
           "0";
          ];

    gr_i=['txt=[''ECLIPSE_MODEL''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 4],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction
