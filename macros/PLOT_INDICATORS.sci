//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2013 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=PLOT_INDICATORS(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Accept inherited events (0/1)';
                'Names';
		'Positive level';
		'Negative level';
                'Positive, neutral, and negative color'];
      types = list('col', 1, 'str', 1, 'col', -1, 'col', -1, 'str', 1);
      [ok ,herited, names_str, pos_level, neg_level, colors_str, exprs] = scicos_getvalue(..
       "Set PLOT_INDICATORS block parameters",labels, types, exprs);

      try
	names = evstr(names_str);
      catch
        message('Names field must contain a matrix of strings.');
        ok=%f;
      end
      try
	colors = evstr(colors_str);
      catch
        message('Colors field must contain a row vector of strings.');
        ok=%f;
      end

      if ~ok then break,end
      mess = [];
      if herited<>0 & herited<>1 then
        mess=[mess;'Accept herited events must have value of either 1 or 0';' '];
        ok=%f;
      end
      if  and(size(colors) <> [1 3]) then
        mess=[mess;'There needs to be 1x3 vector of colors for positive, neutral and negative evel.';' '];
        ok=%f;
      end
      for k=1:size(colors, "*")
        try
	  color(colors(k));
        catch
          mess=[mess;'Each of the colors must be a string accepted by color() function.';' '];
          ok=%f
          break;
        end
      end
      for k=1:size(names, "*")
        if type(names(k)) <> 10 then
          mess=[mess;'Each of the names must be a string';' '];
          ok=%f;
          break;
        end
      end
      //close(f);
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess]);
      end

      if ok then
        [model,graphics,ok] = set_io(model,graphics,list([1 1],..
          [1]),list(),ones(1-herited,1),[])
      end

      if ok then
        model.ipar 	= [herited];
        model.opar 	= list(names, pos_level, neg_level, colors);
        model.evtin	= ones(1-herited,1);
	s 		= size(names);
    	model.in	= ones(s(1),1);
   	model.in2	= ones(s(1),1);
    	model.intyp	= ones(s(1),1);
        graphics.exprs	= exprs;
        x.graphics	= graphics;
        x.model		= model;
        break
      end
    end

   case 'define' then
    herited = 1;
    names = ["GS contact"; "in Sunlight"];
    pos_level = [1;1];
    neg_level = [0;0];
    colors = ["green", "yellow", "red"];
    s 	= size(names);

    model=scicos_model();
    model.sim=list('AB_plot_indicators',5);
    // two input with a single int8 element
    model.in=ones(s(1),1);
    model.in2=ones(s(1),1);
    model.intyp=ones(s(1),1);
    // no outputs
    model.out=[];
    model.out2=[];
    model.outtyp=[];
    
    model.blocktype='d';
    model.dep_ut=[%t %f];

    model.ipar = [herited];
    model.opar = list(names, pos_level, neg_level, colors);

    model.evtin = ones(1-herited,1);

    exprs=[
           string(herited);
           '[""GS contact""; ""in Sunlight""]';
	   '[ 1; 1]';
	   '[ 0; 0]';
	   '[""green"" ""yellow"" ""red""]'
          ];

    gr_i=['txt=[''PLOT_INDICATORS''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

