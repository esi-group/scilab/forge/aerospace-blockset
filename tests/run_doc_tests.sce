// Determining tests to execute

base_path = get_absolute_file_path('run_doc_tests.sce');
s = filesep();

help_files = findfiles(base_path + s + '..' + s + 'help' + s + 'en_US' + s + 'blocks', '*.xml');
number_of_help = size(help_files);
help_files = strsubst(help_files,'.xml','')
help_files = gsort(help_files);

macro_files = findfiles(base_path + s + '..' + s + 'macros', '*.sci');
number_of_macro = size(macro_files);
macro_files = strsubst(macro_files,'.sci','')
macro_files = gsort(macro_files);

block_files = [];
for i =1:number_of_macro(1)
	if(~strcmp(macro_files(i), convstr(macro_files(i), 'u')));
		block_files = [block_files; macro_files(i)];
	end
end
number_of_block = size(block_files);
block_files = gsort(block_files);

//Giving basic info
mprintf("\nThere are %d macro files in macros.", number_of_macro(1));
mprintf("\nThere are %d blocks.", number_of_block(1));
mprintf("\nThere are %d blocks documented in help.", number_of_help(1));
if(and(block_files == help_files))
	mprintf("\nAll blocks are documented.");
else
	error("\nThere is a mismatch between documented and existing blocks.");
end
