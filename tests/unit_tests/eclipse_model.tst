// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for eclipse_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/eclipse_model.zcos");
assert_checktrue(result);

//This diagram uses 7 variables : 
//  obs_pos_in - position of the observer
//  bright_pos_in - position of the eclipsed body
//  dark_pos_in - position of the eclipsing body
//  bright_radius_in - radius of the eclpised body
//  dark_radius_in - radius of the eclipsing body
//  eclipsed_out - fraction of the aparent surface of the "bright" body that is eclipsed by the "dark" one
//  visible_out - fraction of the aparent surface of the "bright" body that is visible from beyond the "dark" one


// ============== Halo case ======================

// Define simulation parameters
obs_pos_in		= [15; 0; 0]
bright_pos_in		= [-5; 0; 0]
dark_pos_in		= [ 8; 0; 0]
bright_radius_in	= 3
dark_radius_in		= 1

// Define expected values
time_expected = [0.1; 1.1; 2.1];
eclipsed_expected = [	0.9065475;  
			0.9065475;  
			0.9065475 ];
visible_expected = [	0.0934525;  
			0.0934525;  
			0.0934525 ];

scicos_simulate(scs_m);

//Validate results
assert_checkequal(eclipsed_out.time, time_expected);
assert_checkequal(visible_out.time, time_expected);
assert_checkalmostequal(eclipsed_out.values, eclipsed_expected, 1e-5);
assert_checkalmostequal(visible_out.values, visible_expected, 1e-5);


// ============== No-eclipse case ======================

// Define simulation parameters
obs_pos_in		= [15; 0; 0]
bright_pos_in		= [-5; 0; 0]
dark_pos_in		= [ 8; 3; 0]
bright_radius_in	= 3
dark_radius_in		= 1

// Define expected values
time_expected = [0.1; 1.1; 2.1];
eclipsed_expected = [	0;  
			0;  
			0 ];
visible_expected = [	1;  
			1;  
			1 ];

scicos_simulate(scs_m);

//Validate results
assert_checkequal(eclipsed_out.time, time_expected);
assert_checkequal(visible_out.time, time_expected);
assert_checkalmostequal(eclipsed_out.values, eclipsed_expected, 1e-5);
assert_checkalmostequal(visible_out.values, visible_expected, 1e-5);


// ============== Coliding bodies case ======================

// Define simulation parameters
// obs_pos_in		= [15; 0; 0]
// bright_pos_in		= [-5; 0; 0]
// dark_pos_in		= [-4; 3; 0]
// bright_radius_in	= 3
// dark_radius_in		= 1

// Define expected values
// time_expected = [0.1; 1.1; 2.1];
// eclipsed_expected = [	0;  
//			0;  
//			0 ];
// visible_expected = [	1;  
//			1;  
//			1 ];

// scicos_simulate(scs_m);

//Validate results
// assert_checkequal(eclipsed_out.time, time_expected);
// assert_checkequal(visible_out.time, time_expected);
// assert_checkalmostequal(eclipsed_out.values, eclipsed_expected, 1e-5);
// assert_checkalmostequal(visible_out.values, visible_expected, 1e-5);
