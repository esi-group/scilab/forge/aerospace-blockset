// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for rad2deg tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/rad2deg.zcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  rad_angle : angle in radians (from_workspace)
//  deg_angle : angle in degrees (to_workspace)

rad_angle = [0, -%pi; 2 * %pi, -4 * %pi];

// Clear variables used for output
clear('deg_angle');

// Simulate
scicos_simulate(scs_m);
exp_value = hypermat([2 2 3]);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_value(:,:,1) = [0, -180; 360, -720];
exp_value(:,:,2) = [0, -180; 360, -720];
exp_value(:,:,3) = [0, -180; 360, -720];

//Validate results
assert_checkequal(exp_time, deg_angle.time);
assert_checkequal(exp_value, deg_angle.values);


//Load xcos test diagram for deg2rad tests
result = importXcosDiagram(path + "../tests/unit_tests" +"/deg2rad.zcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  deg_angle : angle in degrees (from_workspace)
//  rad_angle : angle in radians (to_workspace)

deg_angle = [0, -180; 360, -720];

// Clear variables used for output
clear('rad_angle');

// Simulate
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_value(:,:,1) = [0, -%pi; 2 * %pi, -4 * %pi];
exp_value(:,:,2) = [0, -%pi; 2 * %pi, -4 * %pi];
exp_value(:,:,3) = [0, -%pi; 2 * %pi, -4 * %pi];

//Validate results
assert_checkequal(exp_time, rad_angle.time);
assert_checkequal(exp_value, rad_angle.values);
