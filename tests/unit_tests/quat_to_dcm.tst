// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for quat_to_dcm tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/quat_to_dcm.zcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  q_in : input orientation quaternion (from_workspace)
//  qdot_in : input orientation quaternion derivative (from_workspace)
//  dcm_out : output transformation matrix (to_workspace)
//  omega_out : output angular rate (to_workspace)
//  dcm_out2 : output transformation matrix for simlified block (to_workspace)


// Generate expected data and inputs
exp_time = [0; 1; 2];
ang = CL_deg2rad(10);
exp_dcm = CL_rot_angles2matrix(3,ang) // rotation around Z axis
exp_omega = [1;2;3];
[q_in_struc qdot_in_struc] = CL_rot_matrix2quat(exp_dcm, exp_omega);
q_in = [q_in_struc.r(1); q_in_struc.i(1); q_in_struc.i(2); q_in_struc.i(3)];
qdot_in = [qdot_in_struc.r(1); qdot_in_struc.i(1); qdot_in_struc.i(2); qdot_in_struc.i(3)];

// Simulate
scicos_simulate(scs_m);


//Validate results
assert_checkequal(exp_time, dcm_out.time);
assert_checkalmostequal(exp_dcm, dcm_out.values(:,:,1),%eps);
assert_checkalmostequal(exp_dcm, dcm_out.values(:,:,2),%eps);
assert_checkalmostequal(exp_dcm, dcm_out.values(:,:,3),%eps);

assert_checkalmostequal(exp_omega', omega_out.values(1,:),%eps);
assert_checkalmostequal(exp_omega', omega_out.values(2,:),%eps);
assert_checkalmostequal(exp_omega', omega_out.values(3,:),%eps);

assert_checkalmostequal(exp_dcm, dcm_out2.values(:,:,1),%eps);
assert_checkalmostequal(exp_dcm, dcm_out2.values(:,:,2),%eps);
assert_checkalmostequal(exp_dcm, dcm_out2.values(:,:,3),%eps);

