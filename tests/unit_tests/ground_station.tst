// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for ground_station tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/ground_station.xcos");
assert_checktrue(result);

//This diagram uses 5 variables : 
//    station_name - name of the ground station
//    station_long - longitude of the ground station
//    station_lat  - latitude of the ground station
//    station_alt  - altitude of the ground station
//    station_mask - mask of the ground station

// Define input values
station_name = "Warsaw";
station_long = 21.0108;
station_lat  = 52.2300;
station_alt  = 105;
station_mask = 4;

// Define expected values
exp_name = "Warsaw";
exp_long = 0.3667076;
exp_lat  = 0.9115855;
exp_alt  = 105;
exp_mask = 0.0698132;
expected = [exp_lat, exp_long, exp_alt, exp_mask];
exp_time = 0.1;

//simulate
scicos_simulate(scs_m);
values = [station_out.values];
time = [station_out.time];


//Validate results
assert_checkalmostequal(values, expected,1e-6);
assert_checkequal(time, exp_time);
