// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for vector_normalization tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/vector_normalization.xcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  vectors_in : input vectors (from_workspace)
//  vectors_out : output vectors (to_workspace)

vectors_in = [	0,   1,  -1,  0.1;
		0,  -1,   1,  0.2;
		0.123,   1,   0,  0.3];

scicos_simulate(scs_m);

// Define expected values
exp_time = [0; 1; 2];
exp_value = [	0,    0.5773503,  - 0.7071068,    0.2672612;  
		0,  - 0.5773503,    0.7071068,    0.5345225;  
		1,    0.5773503,    0.,           0.8017837]; 

//Validate results
assert_checkequal(vectors_out.time, exp_time);
assert_checkalmostequal(vectors_out.values(:,:,1), exp_value, 1e-4);
assert_checkalmostequal(vectors_out.values(:,:,2), exp_value, 1e-4);
assert_checkalmostequal(vectors_out.values(:,:,3), exp_value, 1e-4);
